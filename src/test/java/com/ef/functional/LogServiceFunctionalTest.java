package com.ef.functional;

import com.ef.config.LogConfig;
import com.ef.model.LogOccurrence;
import com.ef.model.enums.Duration;
import com.ef.repository.BlockedIpRepository;
import com.ef.repository.LogRepository;
import com.ef.service.LogService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogServiceFunctionalTest {

    @MockBean
    private BlockedIpRepository blockedIpRepository;
    @MockBean
    private JdbcTemplate jdbcTemplate;
    @MockBean
    private DataSource dataSource;
    @MockBean
    private LogRepository logRepository;
    @SpyBean
    private LogConfig logConfig;
    @Autowired
    @InjectMocks
    private LogService logService;

    private List<LogOccurrence> logOccurrences;

    @BeforeClass
    public static void setUpEnv() {
        System.setProperty("filePath", "test");
        System.setProperty("importLogs", "false");
        System.setProperty("threshold", "100");
        System.setProperty("startDate", "2017-01-01.13:00:00");
        System.setProperty("duration", "hourly");
    }

    @Before
    public void setUp(){
        logOccurrences = new ArrayList<>();
        logOccurrences.add(LogOccurrence.builder().ip("1.2.3.4").count(123).build());
        logOccurrences.add(LogOccurrence.builder().ip("1.3.4.4").count(222).build());
        doReturn(Duration.hourly).when(logConfig).getDuration();
        doReturn(100).when(logConfig).getThreshold();
        doReturn("2017-01-01.13:00:00").when(logConfig).getStartDate();
    }

    @Test
    // importLogs = false
    public void testShouldExecuteZeroTimesImportLogsAndSeveralTimesGetMaxOccurrences() {
        //Mockito.doNothing().when(blockedIpRepository).insertBlockedIp(anyString(), anyString(), anyInt());
        doReturn(logOccurrences).when(logRepository).getMaxIpsOccurrences(any(), any(), anyInt());
        logService.startUp();
        verify(logRepository, times(0)).insertLog(any());
        verify(blockedIpRepository, times(2)).insertBlockedIp(anyString(), anyString(), anyInt());
        verify(logRepository, times(1)).getMaxIpsOccurrences(any(), any(), anyInt());
    }

    @Test
    // importLogs = false
    public void testShouldExecuteZeroTimesImportLogsAndSeveralTimesGetMaxOccurrences2() {
        //Mockito.doNothing().when(blockedIpRepository).insertBlockedIp(anyString(), anyString(), anyInt());
        logOccurrences.add(LogOccurrence.builder().ip("1.12.3.4").count(444).build());
        logOccurrences.add(LogOccurrence.builder().ip("1.3.40.4").count(333).build());
        doReturn(logOccurrences).when(logRepository).getMaxIpsOccurrences(any(), any(), anyInt());
        logService.startUp();
        verify(logRepository, times(0)).insertLog(any());
        verify(blockedIpRepository, times(4)).insertBlockedIp(anyString(), anyString(), anyInt());
        verify(logRepository, times(2)).getMaxIpsOccurrences(any(), any(), anyInt());
    }


    @Test
    // importLogs = true
    public void testShouldExecuteImportLogs() {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("access.log").getFile());
        //ReflectionTestUtils.setField(LogConfig.class, "filePath", file.getAbsolutePath());
        doReturn(file.getAbsolutePath()).when(logConfig).getFilePath();
        doReturn(true).when(logConfig).isImportLogs();
        logService.startUp();
        verify(logRepository, times(6)).insertLog(any());
    }

}