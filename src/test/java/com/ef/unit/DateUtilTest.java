package com.ef.unit;

import com.ef.model.enums.Duration;
import com.ef.util.DateUtil;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class DateUtilTest {

    @Test
    public void testGetStartDatePositive(){
        String startDateString = "2017-01-01.13:00:00";
        LocalDateTime startDate = DateUtil.getStartDate(startDateString);
        Assert.assertTrue(startDate.getDayOfMonth()==1);
        Assert.assertTrue(startDate.getMinute()==0);
        Assert.assertTrue(startDate.getMonthValue()==1);
        Assert.assertTrue(startDate.getYear()==2017);
        Assert.assertTrue(startDate.getHour()==13);
    }

    @Test(expected = java.time.format.DateTimeParseException.class)
    public void testGetStartDateNegativeIncorrectFormatWithoutDot(){
        String startDateString = "2017-01-01 13:00:00";
        LocalDateTime startDate = DateUtil.getStartDate(startDateString);
    }

    @Test(expected = java.time.format.DateTimeParseException.class)
    public void testGetStartDateNegativeIncorrectFormatWrongHour(){
        String startDateString = "2017-01-01.56:00:00";
        LocalDateTime startDate = DateUtil.getStartDate(startDateString);
    }

    @Test
    public void testGetEndDatePositiveDaily(){
        String startDateString = "2017-01-01.13:00:00";
        Duration duration = Duration.daily;
        LocalDateTime startDate = DateUtil.getStartDate(startDateString);
        LocalDateTime endDate = DateUtil.getEndDate(startDate, duration);
        Assert.assertTrue(endDate.getDayOfMonth()==2);
        Assert.assertTrue(endDate.getMinute()==0);
        Assert.assertTrue(endDate.getMonthValue()==1);
        Assert.assertTrue(endDate.getYear()==2017);
        Assert.assertTrue(endDate.getHour()==13);
    }

    @Test
    public void testGetEndDatePositiveHourly(){
        String startDateString = "2017-01-01.13:00:00";
        Duration duration = Duration.hourly;
        LocalDateTime startDate = DateUtil.getStartDate(startDateString);
        LocalDateTime endDate = DateUtil.getEndDate(startDate, duration);
        Assert.assertTrue(endDate.getDayOfMonth()==1);
        Assert.assertTrue(endDate.getMinute()==0);
        Assert.assertTrue(endDate.getMonthValue()==1);
        Assert.assertTrue(endDate.getYear()==2017);
        Assert.assertTrue(endDate.getHour()==14);
    }


}
