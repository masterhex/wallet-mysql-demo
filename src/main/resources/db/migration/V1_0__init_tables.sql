CREATE TABLE t_logs
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    ip VARCHAR(45) NOT NULL,
    method VARCHAR(255) NOT NULL,
    response_code VARCHAR(255) NOT NULL,
    client VARCHAR(255) NOT NULL
);
CREATE INDEX t_logs_date_ix ON t_logs (date);
CREATE INDEX t_logs_ip_ix ON t_logs (ip);

CREATE TABLE t_blocked_ips
(
    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    ip VARCHAR(45) NOT NULL,
    occurrences_count INT,
    description VARCHAR(255) NOT NULL
);