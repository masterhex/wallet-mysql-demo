package com.ef.mapper;


import com.ef.model.LogOccurrence;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogOccurenceMapper implements RowMapper{

    @Override
    public LogOccurrence mapRow(ResultSet rs, int i) throws SQLException {
        return LogOccurrence.builder().ip(rs.getString("ip")).count(rs.getInt("count")).build();
    }
}
