package com.ef.repository;

public interface BlockedIpRepository {
    void insertBlockedIp(String ip, String description, int occurrencesCount);
}
