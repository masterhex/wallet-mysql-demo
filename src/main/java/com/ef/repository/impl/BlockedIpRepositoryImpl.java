package com.ef.repository.impl;


import com.ef.repository.BlockedIpRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class BlockedIpRepositoryImpl implements BlockedIpRepository {

    private static final Logger logger = LoggerFactory.getLogger(BlockedIpRepositoryImpl.class);
    private static final String INSERT_BLOCKED_IP = "INSERT INTO t_blocked_ips(ip,description, occurrences_count, date) VALUES(?,?,?,?)";
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void insertBlockedIp(String ip, String description, int occurrencesCount) {
        logger.info("inserting blocked ip = {}, description = {}, occurrencesCount = {}", ip, description, occurrencesCount);
        jdbcTemplate.update(INSERT_BLOCKED_IP, ip, description, occurrencesCount, new Date());
    }
}
