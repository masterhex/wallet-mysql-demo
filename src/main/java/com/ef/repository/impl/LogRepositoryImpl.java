package com.ef.repository.impl;


import com.ef.mapper.LogOccurenceMapper;
import com.ef.model.LogLine;
import com.ef.model.LogOccurrence;
import com.ef.repository.LogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class LogRepositoryImpl implements LogRepository {
    private static final Logger logger = LoggerFactory.getLogger(LogRepositoryImpl.class);
    private static final String INSERT_LOG = "INSERT INTO t_logs(ip,method,client,response_code,date) VALUES(?,?,?,?,?)";
    private static final String GET_MAX_IP_FOR_DURATION = "SELECT ip, count(ip) as count FROM t_logs \n" +
            "WHERE date BETWEEN ? AND ? \n" +
            "GROUP BY ip \n" +
            "HAVING count(ip)>? ;";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void insertLog(LogLine logLine) {
        logger.info("inserting logLine ip = {}", logLine.getIp());
        logger.debug("inserting logLine = {}", logLine);
        jdbcTemplate.update(INSERT_LOG, logLine.getIp(), logLine.getMethod(), logLine.getClient(), logLine.getResponseCode(), logLine.getDate());
    }

    @Override
    public List<LogOccurrence> getMaxIpsOccurrences(LocalDateTime startDate, LocalDateTime endDate, int threshold) {
        logger.info("get max occurrences of ips startDate = {}, endDate = {}, threshold = {}", startDate, endDate, threshold);
        List<LogOccurrence> ips = jdbcTemplate.query(GET_MAX_IP_FOR_DURATION, new Object[]{startDate, endDate, threshold}, new LogOccurenceMapper());
        logger.debug("returned ips from DB = {}", ips);
        return ips;
    }


}
