package com.ef.repository;

import com.ef.model.LogLine;
import com.ef.model.LogOccurrence;

import java.time.LocalDateTime;
import java.util.List;

public interface LogRepository{
    void insertLog(LogLine logLine);
    List<LogOccurrence> getMaxIpsOccurrences(LocalDateTime startDate, LocalDateTime endDate, int threshold);
}
