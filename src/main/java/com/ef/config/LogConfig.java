package com.ef.config;

import com.ef.model.enums.Duration;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;


@Configuration
@Getter
public class LogConfig {
    @Value("${filePath}")
    private String filePath;
    @Value("${startDate}")
    private String startDate;
    @Value("${duration}")
    private Duration duration;
    @Value("${threshold}")
    private int threshold;
    @Value("${importLogs}")
    private boolean importLogs;

    @Bean
    JdbcTemplate jdbcTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
}
