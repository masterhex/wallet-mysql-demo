package com.ef.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LogOccurrence {
    private String ip;
    private int count;
}
