package com.ef.model;


import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LogLine implements Serializable {

    private Long id;
    private String ip;
    private LocalDateTime date;
    private String method;
    private String responseCode;
    private String client;

}