package com.ef.util;

import com.ef.exception.ConflictException;
import com.ef.model.enums.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class DateUtil {
    private static final String PATTERN = "yyyy-MM-dd.HH:mm:ss";
    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

    private DateUtil(){

    }
    public static LocalDateTime getStartDate(String startDateString){
        if(startDateString==null || startDateString.isEmpty()){
            logger.error("startDate is not provided");
            throw new ConflictException("startDate is not provided");
        }
        return LocalDateTime.parse(startDateString, DateTimeFormatter.ofPattern(PATTERN));
    }

    public static LocalDateTime getEndDate(LocalDateTime startDate, Duration duration){
        return duration.equals(Duration.daily) ? startDate.plusDays(1) : startDate.plusHours(1);
    }
}
