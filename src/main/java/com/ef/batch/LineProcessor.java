package com.ef.batch;

import com.ef.model.LogLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemProcessor;

public class LineProcessor implements
        ItemProcessor<LogLine, LogLine>, StepExecutionListener {

    private Logger logger = LoggerFactory.getLogger(LineProcessor.class);

    @Override
    public void beforeStep(StepExecution stepExecution) {
        logger.info("LogLine Processor initialized.");
    }

    @Override
    public LogLine process(LogLine logLine) throws Exception {
        logger.debug("processing logLine {}", logLine);
        return logLine;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        logger.info("LogLine Processor ended.");
        return ExitStatus.COMPLETED;
    }
}
