package com.ef.batch;

import com.ef.model.LogLine;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class FileUtils {

    private final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    private String filePath;
    private CSVReader CSVReader;
    private FileReader fileReader;
    private File file;
    private DateTimeFormatter formatter;

    FileUtils(String filePath) {
        this.filePath = filePath;
    }

    // line example : 2017-01-01 00:00:11.763|192.168.234.82|"GET / HTTP/1.1"|200|"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"
    LogLine readLine() {
        try {
            if (CSVReader == null) initReader();
            String[] line = CSVReader.readNext();
            if (line == null) return null;
            return LogLine.builder()
                    .date(LocalDateTime.parse(line[0], formatter))
                    .ip(line[1])
                    .method(line[2].replaceAll("\"",""))
                    .responseCode(line[3])
                    .client(line[4].replaceAll("\"",""))
                    .build();
        } catch (Exception e) {
            logger.error("Error while reading line in file: {} ", this.filePath);
            return null;
        }
    }

    private void initReader() throws Exception {
        if (file == null) file = new File(filePath);
        if (fileReader == null) fileReader = new FileReader(file);
        if (CSVReader == null) CSVReader = new CSVReader(fileReader, '|');
        if (formatter == null)  formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    }

    void closeReader() {
        try {
            CSVReader.close();
            fileReader.close();
        } catch (IOException e) {
            logger.error("Error while closing reader.");
        }
    }

}
