package com.ef.batch;

import com.ef.model.LogLine;
import com.ef.repository.LogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LinesWriter implements
        ItemWriter<LogLine>, StepExecutionListener {
 
    private final Logger logger = LoggerFactory
      .getLogger(LinesWriter.class);

    @Autowired
    private LogRepository logRepository;
 
    @Override
    public void beforeStep(StepExecution stepExecution) {
        logger.debug("LogLine Writer initialized.");
    }
 
    @Override
    public void write(List<? extends LogLine> lines) throws Exception {
        for (LogLine logLine : lines) {
            logRepository.insertLog(logLine);
            logger.info("Wrote logLine {}", logLine);
        }
    }
 
    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        logger.debug("LogLine Writer ended.");
        return ExitStatus.COMPLETED;
    }
}