package com.ef.batch;

import com.ef.config.LogConfig;
import com.ef.model.LogLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;


public class LineReader implements
        ItemReader<LogLine>, StepExecutionListener {

    private final Logger logger = LoggerFactory.getLogger(LineReader.class);
    @Autowired
    private LogConfig logConfig;

    private FileUtils fu;

    @Override
    public void beforeStep(StepExecution stepExecution) {
        fu = new FileUtils(logConfig.getFilePath());
        logger.debug("LogLine Reader initialized.");
    }

    @Override
    public LogLine read() throws Exception {
        LogLine logLine = fu.readLine();
        if (logLine != null) logger.info("Read logLine: {}", logLine);
        return logLine;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        fu.closeReader();
        logger.debug("LogLine Reader ended.");
        return ExitStatus.COMPLETED;
    }
}