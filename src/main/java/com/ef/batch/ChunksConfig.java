package com.ef.batch;

import com.ef.model.LogLine;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ChunksConfig {
 
    @Autowired
    private JobBuilderFactory jobs;
 
    @Autowired
    private StepBuilderFactory steps;
 
    @Bean
    public ItemReader<LogLine> itemReader() {
        return new LineReader();
    }
 
    @Bean
    public ItemProcessor<LogLine, LogLine> itemProcessor() {
        return new LineProcessor();
    }
 
    @Bean
    public ItemWriter<LogLine> itemWriter() {
        return new LinesWriter();
    }
 
    @Bean
    protected Step processLines(ItemReader<LogLine> reader,
                                ItemProcessor<LogLine, LogLine> processor,
                                ItemWriter<LogLine> writer) {
        return steps.get("processLines").<LogLine, LogLine> chunk(2)
          .reader(reader)
          .processor(processor)
                .writer(writer)
          .build();
    }
 
    @Bean
    public Job job() {
        return jobs
          .get("chunksJob")
          .start(processLines(itemReader(), itemProcessor(), itemWriter()))
          .build();
    }
 
}