package com.ef.service;


import com.ef.config.LogConfig;
import com.ef.model.LogOccurrence;
import com.ef.model.enums.Duration;
import com.ef.repository.BlockedIpRepository;
import com.ef.repository.LogRepository;
import com.ef.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class LogService {
    private static final Logger logger = LoggerFactory.getLogger(LogService.class);
    private static final String REASON = "Too many requests - %s";
    @Autowired
    private LogConfig logConfig;
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private Job job;
    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BlockedIpRepository blockedIpRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void startUp() {
        logger.info("duration {}, threshold {}, startDate {}, log file {}",
                logConfig.getDuration(), logConfig.getThreshold(), logConfig.getStartDate(), logConfig.getFilePath());
        if (logConfig.isImportLogs())
            try {
                logger.info("importLog is true, going to import logs from file");
                jobLauncher.run(job, new JobParameters());
            } catch (Exception e) {
                logger.error("Error executing jobLaucher e = {}", e.getMessage());
            }

        List<LogOccurrence> ips = getMaxIpsOccurrences(logConfig.getStartDate(), logConfig.getDuration(), logConfig.getThreshold());

        ips.forEach(i -> {
            logger.info("Going to insert blocked ip = {}", i);
            String reason = String.format(REASON, i.getCount());
            blockedIpRepository.insertBlockedIp(i.getIp(), reason, i.getCount());
        });
    }

    private List<LogOccurrence> getMaxIpsOccurrences(String startDateString, Duration duration, int threshold){
        LocalDateTime startDate = DateUtil.getStartDate(startDateString);
        LocalDateTime endDate = DateUtil.getEndDate(startDate, duration);
        logger.info("endDate after convert : {}", endDate);
        return logRepository.getMaxIpsOccurrences(startDate, endDate, threshold);
    }

}
