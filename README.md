### Structure 

Used tech stack:
* Spring Boot.
* Spring Batch.
* Spring JDBCTemplate.
* Opencsv reader.

### Build and execution 
**To build the project and execute tests**:<br />
_mvn clean compile package_<br />
**To execute the jar file:**<br />
_java -jar target/mysql-demo.jar --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 --filePath=PATH_TO/access.log --importLogs=false<br />_
`!!!You should have running MySQL database. With credentials and db name as in application.yml. I could have done it more flexible by making outer properties files, but I have chosen this way to make it easer for you to execute.`<br />
**Parameters:**<br />
I have added two additional parameters:
* **filePath** - path to log file to import<br />
* **importLogs** - boolean param import or not import logs<br />

### Short description
* The flyway will create tables.<br />
* The application will go through the log file line by line using Spring Batch (to deal with large files more efficiently).<br />
* The tool will find any IPs that made more than {`threshold`} requests for {`duration`} and print them to console AND also load them to another MySQL table with comments on why it's blocked.
